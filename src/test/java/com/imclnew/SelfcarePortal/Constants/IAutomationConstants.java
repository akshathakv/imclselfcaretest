package com.imclnew.SelfcarePortal.Constants;

public interface IAutomationConstants {
    public static final String CONFIG_PATH = "Config/";
    public static final String CONFIG_FILE = "Config.properties";
    public static final String CROME_DRIVER_PATH = "D:/IdeaProjects/ImclSelfcarePortalNewAutomation/Driver/chromedriver.exe";
    public static final String CHROME_FILE = "chromedriver.exe";
    public static final String FireFox_DRIVER_PATH="D:/IdeaProjects/ImclSelfcarePortalNewAutomation/Driver/geckodriver.exe";
    public static final String FireFox_FILE="geckodriver.exe";
    public static final String IE_DRIVER_PATH="D:/IdeaProjects/ImclSelfcarePortalNewAutomation/Driver/IEDriverServer.exe";
    public static final String IE_FILE="IEDriverServer.exe";

    public static final String REPORT_PATH = "./report/";
    public static final String REPORT_FILE = "report.html";

    public static final String CHROME_KEY = "webdriver.chrome.driver";
    public static final String FireFox_KEY="webdriver.gecko.driver";
    public static final String IE_KEY="webdriver.ie.driver";
}