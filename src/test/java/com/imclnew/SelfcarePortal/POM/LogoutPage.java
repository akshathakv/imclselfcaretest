package com.imclnew.SelfcarePortal.POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LogoutPage extends BasePage {
    public LogoutPage(WebDriver driver)
    {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id=\"bs-example-navbar-collapse-1\"]/ul/li[8]/a")
    private WebElement LogoutHeader;

    public void LogoutFromPortal() throws InterruptedException {
        waitTillTheElementVisible(LogoutHeader);
        Thread.sleep(1000);
        LogoutHeader.click();
    }
}
